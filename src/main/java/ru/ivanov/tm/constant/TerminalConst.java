package ru.ivanov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

}

