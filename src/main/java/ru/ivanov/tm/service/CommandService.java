package ru.ivanov.tm.service;

import ru.ivanov.tm.api.ICommandRepository;
import ru.ivanov.tm.model.Command;
import ru.ivanov.tm.repository.CommandRepository;

public class CommandService implements ru.ivanov.tm.api.ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
